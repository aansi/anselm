import pandas as pd
import numpy as np
import os
import seaborn as sns
sns.set(rc={'figure.figsize':(15, 15)})

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
import xgboost as xgb


#import matplotlib.pyplot as plt

pd.set_option('display.max_rows', 15)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)

os.chdir('Documents/anselm/heart_disease/')

#Data description
"""
https://www.kaggle.com/ronitf/heart-disease-uci

Some of the descriptions were a bit off, more exact descriptions from:
    https://archive.ics.uci.edu/ml/datasets/Heart+Disease

This database contains 76 attributes, but all published experiments refer to using a subset of 14 of them. 
In particular, the Cleveland database is the only one that has been used by ML researchers to this date. 
The "goal" field refers to the presence of heart disease in the patient. It is integer valued from 0 (no presence) to 4.


age         age in years
sex         (1 = male; 0 = female)
cp          chest pain type
                    -- Value 1: typical angina
                    -- Value 2: atypical angina
                    -- Value 3: non-anginal pain
                    -- Value 4: asymptomatic
trestbps    resting blood pressure (in mm Hg on admission to the hospital)
cholserum   cholestoral in mg/dl
fbs         (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false)
restecg     resting electrocardiographic results
                    -- Value 0: normal
                    -- Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
                    -- Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria
thalach     maximum heart rate achieved
exang       exercise induced angina (1 = yes; 0 = no)
oldpeak     ST depression induced by exercise relative to rest
slope       the slope of the peak exercise ST segment
                    -- Value 1: upsloping
                    -- Value 2: flat
                    -- Value 3: downsloping
ca          number of major vessels (0-3) colored by flourosopy
thal        3 = normal; 6 = fixed defect; 7 = reversable defect
target      1 or 0

"""

heartOriginal = pd.read_csv('heart.csv')
heart = heartOriginal.copy(deep = False)

heart.dtypes
heart.head()
heart.describe()
heart.isnull().sum()
heart.isna().sum()

#data cleaning

#It appears there are some 0 here which I'm a bit unsure of. In the dataset archive there is a mention it would be for absence values.
#Check that all values are available.
#set(heart['st_slope'])

categories =  ['sex', 'blood_sugar', 'electrocardio', 'angina', 'st_slope', 'heart_disease', 'thal_defect']

dict_sex = {0:'female', 1:'male'}
dict_blood_sugar = {0:'<=120', 1:'>120'}
dict_electrocardio = {0:'normal', 1:'abnormality', 2:'ventricular hypertrophy'}
dict_angina = {0:'no', 1:'yes'}
dict_st_slope = {0:'missing', 1:'up', 2:'flat', 3:'down'} #has missing
#I'm not sure about the codings here, the values are 0-3. I would assume them to be in order.
dict_thal_defect = {0:'missing', 1:'normal', 2:'fixed defect', 3:'reversable defect'} #has missing
dict_heart_disease = {0:'no', 1:'yes'}

heart.columns = ['age', 'sex', 'chestpain', 'blood_pressure', 'cholestoral', 'blood_sugar', 'electrocardio', 'max_heart_rate', 'angina', 'st_depression', 'st_slope', 'major_vessels', 'thal_defect', 'heart_disease']

heart = heart[(heart['st_slope'] != 0) & (heart['thal_defect'] != 0)]

#['sex', 'blood_sugar', 'electrocardio', 'angina', 'st_slope', 'thal_defect', 'heart_disease']
for toCategorical in categories:
    heart[toCategorical] = heart[toCategorical].astype('category')
    thisDict = locals()['dict_' + toCategorical]
    heart[toCategorical] = heart[toCategorical].apply(lambda x: thisDict[x])
    print(heart[toCategorical].value_counts())
    print('\n')


sns.heatmap(heartOriginal.corr(), annot = True)
#Heart disease(target) seems to have some "clear" correlations with some variables. Good sign for prediction.
sns.countplot(heart['age'])

oh_ec = OneHotEncoder()
tf_oh_ec = oh_ec.fit_transform(heart[categories]) 


#Train test split

X_train, X_test, y_train, y_test = train_test_split(heart, heart['heart_disease'], test_size = 0.2, random_state = 99)
X_train = X_train.drop('heart_disease', 1)
X_test = X_test.drop('heart_disease', 1)

#Tune hyperparameters using gridsearch
RF_grid = {'n_estimators': [50, 100, 200, 500],
           'bootstrap': [True, False],
           'max_features': ['auto', 'sqrt'],
           'max_depth' : [3,4,5,6,7,8],
           'min_samples_split': [2,4,6]
        }



model_RF = RandomForestClassifier()
gridsearch  = GridSearchCV(model_RF, RF_grid, cv = 5, n_jobs = -1)
gridsearch.fit(X_train, y_train)

gridsearch.best_params_
best_RF = gridsearch.best_estimator_


predictions = best_RF.predict(X_test)
predictions_prob = best_RF.predict_proba(X_test)[:,1]

X_test['target'] = y_test
X_test['prediction'] = predictions
X_test['prediction_prob'] = predictions_prob

confusion_matrix(predictions, y_test)

#Misclassification
X_test[X_test['target'] != X_test['prediction']].shape[0] / X_test.shape[0]


'''
Gradient boosting (xgb)
'''

Dmatrix_train = xgb.DMatrix(X_train, label=y_train)
Dmatrix_test = xgb.DMatrix(X_train, label=y_test)

xgb_grid = {"learning_rate"    : [0.01, 0.05, 0.10, 0.15, 0.2] ,
            "max_depth"        : [3, 4, 5 ,6, 7, 8, 9],
            "min_child_weight" : [1, 2, 3, 4, 5],
            "gamma"            : [0.05, 0.1, 0.15, 0.2, 0.25],
            "colsample_bytree" : [0.2, 0.3, 0.4, 0.5, 0.6]}

xgb_classify = xgb.XGBClassifier()

gridsearch_xgb = GridSearchCV(xgb_classify, xgb_grid, cv = 3)
gridsearch_xgb.fit(X_train, y_train)


gridsearch_xgb.best_params_
best_xgb = gridsearch_xgb.best_estimator_


predictions = best_xgb.predict(X_test)
predictions_prob = best_xgb.predict_proba(X_test)[:,1]
#cant be right...
confusion_matrix(y_test, predictions)
tn, fp, fn, tp = confusion_matrix(y_test, predictions).ravel()
print(tn, fp, fn, tp)

fpr, tpr, thresholds = roc_curve(y_test, predictions_prob)
pyplot.plot(fpr, tpr, linestyle='--', label='Succ')
# axis labels
pyplot.xlabel('False Positive Rate')
pyplot.ylabel('True Positive Rate')
# show the legend
pyplot.legend()
# show the plot
pyplot.show()


















