from bs4 import BeautifulSoup
from requests import get
import pandas as pd
import re

import time
import os

os.chdir('/home/pi/scripts/polttoaine/')

url = 'https://www.polttoaine.net/index.php?cmd=kaikki'

page = get(url)
soup = BeautifulSoup(page.text, 'html.parser')
soupTable = soup.find('table')

currCity = None #still not assigned

df = pd.DataFrame(columns = ['ID', 'city', 'fullAd', 'ad1', 'ad2', 'ad3', 'date', 'e95', 'e98', 'diesel'])

for tr in soupTable.findAll('tr')[4:]:

    cityTest = tr.find('b')



    if(cityTest != None):
        currCity = cityTest.string.strip()
        continue

    elif(currCity == None):
        #still not assigned
        continue

    elif(len(tr) == 5):

        try:
            ID = tr.find('a', href=True).get('href')
            ID = ''.join(re.findall('id=(.*)$', ID))

        except:
            ID = ''

        adress, date, e95, e98, diesel = tr.findAll('td')
        #adress = adress.contents[2].split(',')


        adress = adress.contents[min(len(adress.contents)-1, 2)].split(',')
        #adress = adress.contents[2].split(',')

        rawAd = ','.join(adress)

        while(len(adress) < 3):
           adress.append('')

        ad1 = adress[0].strip()
        ad2 = adress[1].strip()
        ad3 = adress[2].strip()

        date = date.string.strip()
        e95 = e95.string.strip()
        if e98.string == None:
            e98 = ''
        else:
            e98 = e98.string.strip()
        diesel = diesel.string.strip()


        rowToAdd = pd.Series([ID, currCity, rawAd, ad1, ad2, ad3, date, e95, e98, diesel],
                             ['ID', 'city', 'fullAd', 'ad1', 'ad2', 'ad3', 'date', 'e95', 'e98', 'diesel'])

        df = df.append(rowToAdd, ignore_index = True)

timeNow = time.strftime('%Y-%m-%d %H:%M:%S')

df = df.assign(time = timeNow)

pd.DataFrame.to_csv(df, 'PA_'+time.strftime('%Y%m%d_%H%M')+'.csv', index = False)


fullData = pd.read_csv('FULLDATA.csv', dtype = object)




test = pd.merge(df, fullData, how = 'outer', on = ['ID', 'date'], indicator = True).query('_merge == "left_only"')
test = test[['ID', 'city_x', 'fullAd_x', 'ad1_x', 'ad2_x', 'ad3_x', 'date', 'e95_x', 'e98_x', 'diesel_x', 'time_x']]
test.columns = ['ID', 'city', 'fullAd', 'ad1', 'ad2', 'ad3', 'date', 'e95', 'e98', 'diesel', 'time']



fullData = fullData.append(test)

pd.DataFrame.to_csv(fullData, 'FULLDATA.csv', index = False)


