import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)

#import numpy as np
import seaborn as sns

import requests
import bs4

import re
#from nltk import word_tokenize, pos_tag
from nltk.stem import porter, lancaster

from wordcloud import STOPWORDS

from collections import Counter

import os

#data from https://www.kaggle.com/zynicide/wine-reviews#winemag-data-130k-v2.csv
os.chdir('C:/Users/anssi.lindblom/Documents/anselm/sommelier')
os.listdir()

wine = pd.read_csv('winemag-data-130k-v2.csv')

wine.info()
wine.head()

#count missing
len(wine) - wine.count()
#Just making sure we have some Italian wines included... Mmmmm
wine.country.value_counts()

#I really need the price.
wine = wine.dropna(subset=['price'])
#TODO Imputation would be challenging to do, could however flag estimated prices if used.
wine.price = wine.price.astype(int)

#Normal people dont buy bottles worth more than 25, 30 max!
sns.distplot(wine.price[wine.price < 100], kde = False, bins = 20)
sns.countplot(wine.points)

#Make some use of "REAL WINE" (?) descriptions also.
res = requests.get('https://en.wikipedia.org/wiki/Wine_tasting_descriptors')
res.raise_for_status()
wiki = bs4.BeautifulSoup(res.text, 'html.parser')
wiki_taste = list()
for ul in wiki.find_all('ul'):
    for li in ul.find_all('li'):
        taste = li.find('b')
        if taste != None:
            wiki_taste.append(taste.getText('b'))


#Same clean up for wiki_taste as for description later.
PS = porter.PorterStemmer()
LS = lancaster.LancasterStemmer()
            
wiki_taste_edit = [word.split(' ')[0] for word in wiki_taste]
wiki_taste_edit = [PS.stem(str.lower(word)) for word in wiki_taste_edit]
wiki_taste_edit = set(wiki_taste_edit)

#Clean up description. Keep original
#I don't really like having lists in column, but I think in this case it is a bit cleaner (more difficult to work with though)

wine['descriptionEdit']= wine['description']
wine['descriptionEdit'] = wine['descriptionEdit'].str.lower()
#Only keep A-Z and whitespace
wine['descriptionEdit'] = wine['descriptionEdit'].str.replace('[^a-z ]', '')

#TESTING. This wasn't necessary as I decided to go with Stopwords + stemming + wiki descriptions.
#Could clean up descptions using tags. However the descriptions will be cleaned up and only keep wine terms scraped from wiki.
#test = wine.head()
#test['tokens'] = test['descriptionEdit'].apply(word_tokenize)
#test['tagged'] = test['tokens'].apply(pos_tag)

#Same cleaning/formatting for the stopwords as for descriptions and wiki_tastes... Not sure if all words in lower so lets ensure they are
stopwords_english = [x.lower() for x in STOPWORDS]
stopwords_english = [re.sub('[^a-z]', '', x) for x in stopwords_english]

#remove stopwords
wine['descriptionEdit'] = wine['descriptionEdit'].apply(lambda x: [word for word in str(x).split(' ') if word not in stopwords_english])
#Stem
wine['descriptionEdit'] = wine['descriptionEdit'].apply(lambda x: [PS.stem(word) for word in x])
#Keep original and make new with only wiki_taste matches
wine['descriptionEdit2'] = wine['descriptionEdit'].apply(lambda x: [word for word in set(x) if word in wiki_taste_edit])

#count popular words. Checking if we have anything that could be of use.
longlistofwords = []
wine['descriptionEdit'].apply(lambda x :[longlistofwords.append(item) for item in x])

counts = Counter(longlistofwords)
wordoccurence = pd.DataFrame.from_dict(counts, orient='index').reset_index()
wordoccurence = wordoccurence.rename(columns={'index':'word', 0:'count'})
del(counts)

wordoccurence['count'] = wordoccurence['count'].apply(pd.to_numeric)
wordoccurence = wordoccurence.sort_values('count', ascending = False)
wordoccurence = wordoccurence[wordoccurence['count'] > 100]

sns.distplot(wine['descriptionEdit'].apply(len), kde = False, bins = 20)
sns.countplot(wine['descriptionEdit2'].apply(len))


print(wine[wine.description.str.contains("fallen over", case = False)])

#TESTING
#Fill descriptions
usertaste = ['aroma', 'finish', 'herbal']
#Select your favorite wine
usertaste = wine['descriptionEdit2'][(wine['title'].str.contains('riscal', case = False)) & (wine['designation'] == 'Gran Reserva')].tolist()[0]


#Stem just to be sure
usertasteStem = [PS.stem(str.lower(word)) for word in usertaste]

#Check how many usertaste can be found in descriptionEdit2. Need to do some testing which setting would be best.
#PS/LS stemming or edit/edit2. I expect Edit2 with PS would produce more exact matches, but in fewer cases.

wine['matches'] = wine['descriptionEdit2'].apply(lambda x: sum([choice in usertasteStem for choice in x]))
#Require atleast 2 matches, which also is quite low.
wine[wine['matches'] >= 2].sort_values('matches', ascending = False)

#TODO Put into a neat shiny app with text search => Stem+format => Match => suggest :)
#TODO Make a user profile. Select your favorite wines, figure out variety+description combinations. Suggest similar. Compare price also.



    
    
    

